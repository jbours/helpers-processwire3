<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use JBours\Helpers\Value;
use function ProcessWire\wire;

class Foundation6Menu
{
    /**
     * Set the matching menu item active
     *
     * @param mixed $a
     * @param mixed $b
     *
     * @return mixed
     */
    public static function activeMenuItem($a, $b)
    {
        return Value::isMatch($a, $b) ? 'class="active"' : false;
    }

    /**
     * Set the parent of the sub-menu item active
     *
     * @param mixed $a
     * @param mixed $b
     */
    public static function activeSubMenuItem($a, $b): string
    {
        if ($a->children->count) {
            foreach ($a->children as $child) {
                if (Value::isMatch($child, wire()->page)) {
                    echo 'class="active"';
                } else {
                    self::activeMenuItem($a, $b);
                }
            }
        } else {
            self::activeMenuItem($a, $b);
        }

        return '';
    }
}
