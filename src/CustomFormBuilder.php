<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use function in_array;
use function ProcessWire\wireInput;

class CustomFormBuilder
{
    /**
     * Get the field value by name and request method
     *
     * @return mixed|null
     */
    public static function getFieldValue(?string $fieldName = null)
    {
        if ($fieldName) {
            if (in_array($_SERVER['REQUEST_METHOD'], ['POST', 'PUT', 'PATCH', 'DELETE'])) {
                return wireInput()->post->{$fieldName};
            }

            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                return wireInput()->get->{$fieldName};
            }
        }

        return null;
    }
}
