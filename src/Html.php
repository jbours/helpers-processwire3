<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use function in_array;
use function ProcessWire\wire;
use function sprintf;
use function ucfirst;

class Html
{
    /**
     * Renders a basic key/value (html)table
     *
     * @param array|mixed $array
     * @param array<string> $exclude
     */
    public static function renderKeyValueTable($array, array $exclude = [], bool $translate = false): string
    {
        $html = '<table>';

        foreach ($array as $key => $value) {
            if (in_array($key, $exclude, true)) {
                continue;
            }

            if ($translate) {
               $key = wire('translator')->trans("form.{$key}");
            }

            $html .= sprintf('<tr><td><strong>%s</strong></td><td>%s</td></tr>', ucfirst($key), $value);
        }

        $html .= '</table>';

        return $html;
    }
}
