<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use ProcessWire\Pageimage;

class Image
{
    /**
     * Fetch the alt text or the image name
     */
    public static function getAltText(Pageimage $image, ?string $fallback = null): string
    {
        if (!empty($image->description)) {
            return $image->description;
        }

        if (!empty($fallback)) {
            return $fallback;
        }

        return $image->basename;
    }
}
