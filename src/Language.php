<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use JBours\Helpers\Value;

use function ProcessWire\_x;
use function ProcessWire\page;
use function ProcessWire\user;
use function ProcessWire\wire;

class Language
{
    /**
     * @var string
     */
    protected static $name = 'nl_NL';

    /**
     * @return string
     */
    public static function getActive(): string
    {
        return wire('user')->language->name !== 'default' ? wire('user')->language->name : self::$name;
    }

    public static function setDefaultName(string $tag): void
    {
        self::$name = $tag;
    }

    /**
     * @param $language
     */
    public static function setPageLanguage($language): void
    {
        if (!Value::isMatch(user()->language, $language) && page()->viewable($language)) {
            user()->language = $language;
        }
    }

    /**
     * handles translations in a single file instead of each template on it's own.
     */
    public static function _t(
        string $text,
        string $context = 'General',
        string $textDomain = '/site/templates/language/strings.php'
    ): string {
        return _x($text, $context, $textDomain);
    }
}
