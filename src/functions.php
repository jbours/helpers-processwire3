<?php

declare(strict_types=1);

use JBours\Helpers\ProcessWire\CustomFormBuilder;
use JBours\Helpers\ProcessWire\Foundation6ExceptionCallout;
use JBours\Helpers\ProcessWire\Foundation6Menu;
use JBours\Helpers\ProcessWire\Html;
use JBours\Helpers\ProcessWire\Image;
use JBours\Helpers\ProcessWire\Language;
use JBours\Helpers\ProcessWire\MatrixRepeater;
use JBours\Helpers\ProcessWire\MenuBuilder;
use JBours\Helpers\ProcessWire\Sanitizer;
use ProcessWire\Page;
use ProcessWire\Pageimage;
use ProcessWire\WireException;
use ProcessWire\WirePermissionException;
use function ProcessWire\__;

if (!function_exists('youtubeOrImageUrl')) {
    /**
     * @param object|stdClass|Page $object
     *
     * @throws WireException
     * @throws WirePermissionException
     */
    function youtubeOrImageUrl($object, ?int $width = null, ?int $height = null, ?int $placeholderImagePageId = null): string
    {
        return MatrixRepeater::youtubeOrImageUrl($object, $width, $height, $placeholderImagePageId);
    }
}


if (!function_exists('getFieldValue')) {
    /**
     * @return mixed|null
     */
    function getFieldValue(?string $fieldName = null)
    {
        return CustomFormBuilder::getFieldValue($fieldName);
    }
}

if (!function_exists('getActiveLanguage')) {
    function getActiveLanguage(): string
    {
        return Language::getActive();
    }
}

if (!function_exists('changeUserLanguage')) {
    /**
     * @param $language
     */
    function changeUserLanguage($language)
    {
        Language::setPageLanguage($language);
    }
}

if (!function_exists('repopulateMenuItems')) {
    /**
     * @param $menuItems
     *
     * @return array
     */
    function repopulateMenuItems($menuItems): array
    {
        return MenuBuilder::repopulateMenu($menuItems);
    }
}


if (!function_exists('populateMenuItem')) {
    /**
     * @param $menuItem
     *
     * @return array
     */
    function populateMenuItem($menuItem): array
    {
        return MenuBuilder::repopulateItem($menuItem);
    }
}

if (!function_exists('activeMenuItem')) {
    /**
     * @param $a
     * @param $b
     *
     * @return mixed
     */
    function activeMenuItem($a, $b)
    {
        return Foundation6Menu::activeMenuItem($a, $b);
    }
}

if (!function_exists('activeSubMenuItem')) {
    /**
     * @param mixed $a
     * @param mixed $b
     */
    function activeSubMenuItem($a, $b)
    {
        Foundation6Menu::activeSubMenuItem($a, $b);
    }
}

if (!function_exists('boolForHuman')) {
    function boolForHuman(bool $bool): string
    {
        return $bool === true ? __('yes') : __('no');
    }
}

if (!function_exists('sanitizeSearchQuery')) {
    function sanitizeSearchQuery(string $query): string
    {
        return Sanitizer::searchQuery($query);
    }
}

if (!function_exists('renderKeyValueTable')) {
    /**
     * Renders a basic key/value (html)table
     *
     * @param $array
     * @param array $exclude
     * @param bool $translate
     *
     * @return string
     */
    function renderKeyValueTable($array, array $exclude = [], $translate = false): string
    {
        return Html::renderKeyValueTable($array, $exclude, $translate);
    }
}

if (!function_exists('_t')) {
    function _t(string $text, string $context, string $textDomain = '/site/templates/language/strings.php'): string
    {
        return Language::_t($text, $context, $textDomain);
    }
}

if (!function_exists('foundationExceptionCallOut')) {
    function foundationExceptionCallOut(Throwable $e, string $level = 'primary'): string
    {
        return Foundation6ExceptionCallout::render($e, $level);
    }
}

if (!function_exists('getImageAlt')) {
    function getImageAlt(Pageimage $image, string $backupText = ''): string
    {
        return Image::getAltText($image, $backupText);
    }
}
