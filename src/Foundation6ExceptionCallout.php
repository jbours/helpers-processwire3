<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use Throwable;

use function sprintf;

class Foundation6ExceptionCallout
{
    /**
     * Renders a Foundation 6 Callout with a PHP Exception message
     */
    public static function render(Throwable $e, string $level = 'primary'): string
    {
        return sprintf('<div class="callout %s">%s</div>', $level, $e->getMessage());
    }
}
