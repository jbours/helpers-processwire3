<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use stdClass;
use Traversable;

class MenuBuilder
{
    /**
     * @param object[]|stdClass[]|Traversable $menuItems
     *
     * @return array[]
     */
    public static function repopulateMenu($menuItems): array
    {
        $newItems = [];

        foreach ($menuItems as $menuItem) {
            if ($menuItem->parentID === 0) {
                $newItems[$menuItem->id] = self::repopulateItem($menuItem);
            } elseif ($menuItem->parentID > 0) {
                $newItems[$menuItem->parentID]['children'][] = self::repopulateItem($menuItem);
                if ($menuItem->isCurrent) {
                    $newItems[$menuItem->parentID]['current'] = $menuItem->isCurrent;
                }
            }
        }

        return $newItems;
    }

    /**
     * @param object|stdClass $menuItem
     *
     * @return array
     */
    public static function repopulateItem($menuItem): array
    {
        return [
            'id'        => $menuItem->id,
            'title'     => $menuItem->title,
            'url'       => $menuItem->url,
            'new_tab'   => $menuItem->newtab,
            'page_id'   => $menuItem->pagesID,
            'css_id'    => $menuItem->cssID,
            'css_class' => $menuItem->cssClass,
            'parent'    => $menuItem->isParent,
            'current'   => $menuItem->isCurrent,
        ];
    }
}
