<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use ProcessWire\Page;
use ProcessWire\Pageimage;
use ProcessWire\WireException;
use ProcessWire\WirePermissionException;
use stdClass;

use function ProcessWire\wire;

class MatrixRepeater
{
    /**
     * Returns the youtube thumbnail or the (resized) (dummy)image(s)
     *
     * @param object|stdClass|Page $object
     * @throws WireException
     * @throws WirePermissionException
     */
    public static function youtubeOrImageUrl(
        $object,
        ?int $width = null,
        ?int $height = null,
        ?int $placeholderImagePageId = null
    ): string {
        $image = null;

        if ($object) {
            return self::handleObject($object, $width, $height);
        }

        return self::handleImage(wire('pages')->get($placeholderImagePageId)->get('image'), $width, $height);
    }

    /**
     * @param object|stdClass|Page $object
     */
    private static function handleObject($object, ?int $width = null, ?int $height = null): string
    {
        switch ($object->type) {
            case 'image_slide':
                return self::handleImage($object->image, $width, $height);
            case 'youtube_slide':
                return 'http://img.youtube.com/vi/' . $object->youtube_url . '/0.jpg';
            default:
                return '';
        }
    }

    private static function handleImage(Pageimage $image, ?int $width = null, ?int $height = null): string
    {
        if ($width || $height) {
            return $image->size($width, $height)->url;
        }

        return $image->url;
    }
}
