<?php

declare(strict_types=1);

namespace JBours\Helpers\ProcessWire;

use function implode;
use function preg_split;
use function str_replace;
use function strip_tags;

use const PREG_SPLIT_NO_EMPTY;

class Sanitizer
{
    /**
     * Sanitize the search query
     */
    public static function searchQuery(string $query): string
    {
        $clean = strip_tags($query);
        $clean = str_replace(',', '', $clean);
        $clean = preg_split('/\s+/', $clean, -1, PREG_SPLIT_NO_EMPTY);
        $clean = implode('|', $clean);

        return $clean;
    }
}
