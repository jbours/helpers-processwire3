# ProcessWire 3 helpers
this package provide some addional function which may come in handy when developing a ProcessWire3 website.

---
## Available Functions ##
* [youtubeOrImageUrl](#youtubeorimageurl)
* [getFieldValue](#getfieldvalue)
* [getActiveLanguage](#getactivelanguage)
* [changeUserLanguage](#changeuserlanguage)
* [populateMenuItems](#populatemenuitems)
* [populateMenuItem](#populatemenuitem)
* [activeMenuItem](#activemenuitem)
* [activeSubMenuItem](#activeSubmenuitem)
* [boolForHuman](#boolforhuman)
* [sanitizeSearchQuery](#sanitizesearchquery)
* [renderKeyValueTable](#renderkeyvaluetable)
* [_t](#_t)
* [getImageAlt](#getimagealt)

### youtubeOrImageUrl [&#x21EB;](#available-functions)
This is a very specific function which only can be used if the project is setup right. Currently this function can be used by MindWorkz ProcessWire3 based websites or my own ProcessWire 3 based projects.

example usage: `echo youtubeOrImageUrl(<repeaterMatrixArray>, <image-with>, <image-height>, <fallback-image-page-id>);`

### getFieldValue [&#x21EB;](#available-functions)
Grabs the GET or POST parameter based on the ProcessWire fieldname.

example use: `$title = getFieldValue('title');`


### getActiveLanguage  [&#x21EB;](#available-functions)
Returns the current active user language.

example use: `$currentLanguage = getActiveLanguage();`

### changeUserLanguage  [&#x21EB;](#available-functions)
Returns the current active user language.

example use: `changeUserLanguage('en');`

### populateMenuItems  [&#x21EB;](#available-functions)
Transform the menu items from MarkupMenuBuilder to an array.

example use: `repopulareMenuItems(<WireArray>);`

### populateMenuItem [&#x21EB;](#available-functions)
Is used by populateMenuItems for transforming an induvidual menu item.

example use: `repopulareMenuItems(<WireArray>);`

### activeMenuItem [&#x21EB;](#available-functions)
Returns class="active" when the page matches with the menu item.

example use: `activeMenuItem(<menuItem>, <currentPage>);`

### activeSubMenuItem [&#x21EB;](#available-functions)
Returns class="active" when the page matches with the menu item.

example use: `activeSubMenuItem(<menuItem>, <currentPage>);`

### boolForHuman  [&#x21EB;](#available-functions)
Returns 'yes' if bool is true. Returns 'no' if bool is false.

example use: `activeSubMenuItem($subMenuItem <Page>, $page <Page>);`

### sanitizeSearchQuery  [&#x21EB;](#available-functions)
Sanitizes the search query and optimizes it for Processwire.

### renderKeyValueTable  [&#x21EB;](#available-functions)
Renders a HTML-table based on the keys and values of an array (only 1 dimension).

example use: `renderKeyValueTable($data <array>, $exclude <array>, $translate <bool>);`

### _t  [&#x21EB;](#available-functions)
wrapper for processwire translation, so the translation can be managed in a single file `site/templates/language/strings.php` and single translation page.

example use: `_t('string', 'text-domain');`

### getImageAlt  [&#x21EB;](#available-functions)
return the alt text for an Pageimage object.

example use: `getImageAlt($image <Pageimage>, 'fallback alt text');`

--- 